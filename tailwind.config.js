/** @type {import('tailwindcss').Config} */
export default {
    content: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
    theme: {
        container: {
            center: true,
            padding: {
                DEFAULT: '1.59375rem',
                '2xl': '2.5rem'
            }
        },
        colors: {
            black: '#222',
            gray: '#19191D',
            gray20: '#F2F5F7',
            gray30: '#b4b4bb',
            gray50: '#787885',
            gray60: '#5A5B6A',
            blue: '#2264D1',
            white: '#fff',
            blue10: '#ebf2ff',
        },
        fontFamily: {
            inter: ['Inter', 'sans-serif'],
            quicksand: ['Quicksand', 'sans-serif'],
            roboto: ['Roboto', 'sans-serif']
        }
    },
    plugins: []
}
