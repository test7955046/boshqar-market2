import '@/style.css'
import { createApp } from 'vue'
import App from './App.vue'
import router from './plugins/router.js'
import store from '@/plugins/vuex/store.js'
import { useI18n } from 'vue-i18n'
import i18n from '@/locales/i18n.js'
import './NProgress.css'

const app = createApp(App, {
    setup() {
        const { t } = useI18n({ useScope: 'global' }) // call `useI18n`, and spread `t` from  `useI18n` returning
        return { t } // return render context that included `t`
    }
})
app.use(router)
app.use(store)
app.use(i18n)

app.mount('#app')
