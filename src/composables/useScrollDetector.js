import { onMounted, onUnmounted, ref } from 'vue'

export default function () {
    const isTop = ref(true)

    const updateIsTopOnScroll = () => {
        window.scrollY > 10 ? (isTop.value = false) : (isTop.value = true)
    }

    onMounted(() => {
        window.addEventListener('scroll', updateIsTopOnScroll)
    })

    onUnmounted(() => {
        window.removeEventListener('scroll', updateIsTopOnScroll)
    })

    return {
        isTop
    }
}
