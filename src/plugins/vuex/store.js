import {createStore} from 'vuex'
import user from '@/plugins/vuex/modules/user.js'

export default createStore({
  modules: [
    user
  ]
})