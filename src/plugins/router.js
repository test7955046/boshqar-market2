import { createRouter, createWebHistory } from 'vue-router'
import NProgress from 'nprogress'

const router = createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: '/',
            name: 'home',
            component: () => import('@/pages/HomePage.vue')
        },
        {
            path: '/auth',
            name: 'auth',
            component: () => import('@/pages/AuthPage.vue')
        },
        {
            path: '/item-info',
            name: 'info',
            component: () => import('@/pages/ItemInfoPage.vue')
        },
        {
            path: '/purchase',
            name: 'purchase',
            component: () => import('@/pages/PurchasePage.vue')
        },
        {
            path: '/basket',
            name: 'basket',
            component: () => import('@/pages/BasketPage.vue')
        },
        {
            path: '/:catchAll(.*)',
            name: 'notFound',
            component: () => import('@/pages/NotFound.vue')
        }
    ],
    scrollBehavior(to) {
        if (to.hash) {
            return {
                el: to.hash,
                behavior: 'smooth'
            }
        }
        return {
            top: 0,
            behavior: 'smooth'
        }
    }
})

export default router

NProgress.configure({ easing: 'ease', speed: 1000, showSpinner: false, trickle: false })

router.beforeEach(() => {
    NProgress.start()
})

router.afterEach(() => {
    NProgress.done()
})
